/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.hw.oop.tictactoe.Player;
import com.hw.oop.tictactoe.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author BenZ
 */
public class testTable {
    
    public testTable() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX(){
        Player x = new Player('X');
        Table table = new Table();
        table.setTable(new char[][]{{'X', 'X', 'X'},
                                    {'-', '-', '-'},
                                    {'-', '-', '-'}});
        
        table.checkWin(0, 2);
        assertEquals(true, table.getFinish());
        assertEquals(x.getName(), table.getWinner().getName());
        
    }
    
    public void testRow2ByO(){
        Player o = new Player('O');
        Table table = new Table();
        table.setTable(new char[][]{{'-', '-', '-'},
                                    {'O', 'O', 'O'},
                                    {'-', '-', '-'}});
        table.switchPlayer();
        table.checkWin(1, 2);
        assertEquals(true, table.getFinish());
        assertEquals(o.getName(), table.getWinner().getName());
        
    }
    public void testRow3ByX(){
        Player x = new Player('X');
        Table table = new Table();
        table.setTable(new char[][]{{'-', '-', '-'},
                                    {'-', '-', '-'},
                                    {'X', 'X', 'X'}});
        table.checkWin(2, 2);
        assertEquals(true, table.getFinish());
        assertEquals(x.getName(), table.getWinner().getName());
        
    }
    
    public void testCol1ByO(){
        Player o = new Player('O');
        Table table = new Table();
        table.setTable(new char[][]{{'O', '-', '-'},
                                    {'O', '-', '-'},
                                    {'O', '-', '-'}});
        table.switchPlayer();
        table.checkWin(2, 0);
        assertEquals(true, table.getFinish());
        assertEquals(o.getName(), table.getWinner().getName());
        
    }
    
    public void testCol2ByX(){
        Player x = new Player('X');
        Table table = new Table();
        table.setTable(new char[][]{{'-', 'X', '-'},
                                    {'-', 'X', '-'},
                                    {'-', 'X', '-'}});
        table.checkWin(2, 1);
        assertEquals(true, table.getFinish());
        assertEquals(x.getName(), table.getWinner().getName());
        
    }
    
    
    public void testCol3ByO(){
        Player o = new Player('O');
        Table table = new Table();
        table.setTable(new char[][]{{'-', '-', 'O'},
                                    {'-', '-', 'O'},
                                    {'-', '-', 'O'}});
        table.switchPlayer();
        table.checkWin(2, 2);
        assertEquals(true, table.getFinish());
        assertEquals(o.getName(), table.getWinner().getName());
        
    }
    public void testCross1ByX(){
        Player x = new Player('X');
        Table table = new Table();
        table.setTable(new char[][]{{'X', '-', '-'},
                                    {'-', 'X', '-'},
                                    {'-', '-', 'X'}});
        table.checkWin(0, 0);
        assertEquals(true, table.getFinish());
        assertEquals(x.getName(), table.getWinner().getName());
        
    }
    
    public void testCross2ByO(){
        Player o = new Player('O');
        Table table = new Table();
        table.setTable(new char[][]{{'O', '-', '-'},
                                    {'-', 'O', '-'},
                                    {'-', '-', 'O'}});
        table.switchPlayer();
        table.checkWin(0, 0);
        assertEquals(true, table.getFinish());
        assertEquals(o.getName(), table.getWinner().getName());
        
    }public void testDrew(){
        
        Table table = new Table();
        table.setTable(new char[][]{{'X', 'O', 'X'},
                                    {'O', 'O', 'X'},
                                    {'X', 'X', 'O'}});
        table.checkWin(0, 0);
        assertEquals(true, table.getFinish());
        assertEquals(null, table.getWinner());
        
    }
}
