/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.oop.tictactoe;

/**
 *
 * @author BenZ
 */
import java.util.*;

public class Game {

    private Table table;
    private int row, col;
    private Scanner kb;

    public Game() {
        setTable(new Table());
        setRow(-1);
        setCol(-1);
        setKb(new Scanner(System.in));
    }

    public Scanner getKb() {
        return kb;
    }

    public void setKb(Scanner kb) {
        this.kb = kb;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public Table getTable() {
        return table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public void run() {
        showWelcome();
        do {
            do {
                getTable().showTable();
                showTurn();
                input();
                getTable().checkWin(getRow(), getCol());
                getTable().switchPlayer();
            } while (!table.getFinish());
            getTable().showTable();
            showResult();
            getTable().clearTable();
        } while (toContinue());
        showScore();
        showBye();

    }

    public boolean toContinue() {
        boolean check = true;
        System.out.println("Do you want to continue playing?(Y/N)");
        do  {
            char ans = kb.next().charAt(0);
            if (ans == 'Y' ||ans == 'y' ) {
                check = false;
                return true;
            }else if(ans == 'N' ||ans == 'n'){
                check = false;
                return false;
            }
            System.out.println("Please enter Y or N");
            System.out.println("Do you want to continue playing?(Y/N)");
        }while(check);
        return false;
    }

    public void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    public void input() {

        while (true) {
            System.out.println("Please input Row Col :");
            setRow(getKb().nextInt() - 1);
            setCol(getKb().nextInt() - 1);
            if (getTable().setRowCol(getRow(), getCol())) {
                break;
            }

            System.out.println("Error : table at row and col is not empty!!!!");
        }
    }

    public void showTurn() {
        System.out.println(getTable().getCurrentPlayer().getName() + " Turn");
    }

    public void showResult() {
        if (getTable().getWinner() != null) {
            System.out.println(getTable().getWinner().getName() + " Win");

        } else {
            System.out.println("Draw!!!");
        }
    }
    public void showScore(){
        System.out.println(getTable().getPlayerO().toString());
        System.out.println(getTable().getPlayerX().toString());
    }

    static void showBye() {
        System.out.println("Bye Bye....");
    }

}
