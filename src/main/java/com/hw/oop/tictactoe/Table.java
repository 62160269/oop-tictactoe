/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.oop.tictactoe;

/**
 *
 * @author BenZ
 */
public class Table {

    private char[][] table;
    private Player currentPlayer, playerX, playerO, winner;
    private boolean finish;
    private int round;

    public Table() {
        setTable(new char[][]{{'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}});
        setPlayerX(new Player('X'));
        setPlayerO(new Player('O'));
        setCurrentPlayer(getPlayerX());
        setFinish(false);
        setWinner(null);
        setRound(0);
    }
    
    public char[][] getTable() {
        return table;
    }

    public void setTable(char[][] table) {
        this.table = table;
    }
    
    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }
    
    public Player getPlayerX() {
        return playerX;
    }

    public void setPlayerX(Player playerX) {
        this.playerX = playerX;
    }
    
    public Player getPlayerO() {
        return playerO;
    }

    public void setPlayerO(Player playerO) {
        this.playerO = playerO;
    }
    
    public Player getWinner() {
        return winner;
    }

    public void setWinner(Player winner) {
        this.winner = winner;
    }
    
    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round += round;
    }

    public boolean getFinish() {
        return finish;
    }

    public void setFinish(boolean finish) {
        this.finish = finish;
    }

    public void showTable() {
        System.out.println("  1 2 3");
        for (int i = 0; i < getTable().length; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < getTable()[i].length; j++) {
                System.out.print(getTable()[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = getCurrentPlayer().getName();
            setRound(1);
            return true;
        }
        return false;
    }

    public void switchPlayer() {
        if (getCurrentPlayer().getName() == 'X') {
            setCurrentPlayer(getPlayerO());
        } else {
            setCurrentPlayer(getPlayerX());
        }
    }

    public void checkWin(int row, int col) {
        checkRow(row);
        checkCol(col);
        checkCross1();
        checkCross2();
        checkDrew();
        inputScore();
    }

    public void inputScore() {
        if (getWinner() != null) {
            if (getWinner().getName() == getPlayerX().getName()) {
                getPlayerX().setWin(1);
                getPlayerO().setLost(1);
            } else if (getWinner().getName() == getPlayerO().getName()) {
                getPlayerO().setWin(1);
                getPlayerX().setLost(1);
            } else if (getFinish()) {
                getPlayerO().setDraw(1);
                getPlayerX().setDraw(1);
            }
        }
    }

    public void checkRow(int row) {
        for (int i = 0; i < 3; i++) {
            if (table[row][i] != getCurrentPlayer().getName()) {
                return;
            }
        }
        setFinish(true);
        setWinner(getCurrentPlayer());
    }

    public void checkCol(int col) {
        for (int i = 0; i < 3; i++) {
            if (table[i][col] != getCurrentPlayer().getName()) {
                return;
            }
        }
        setFinish(true);
        setWinner(getCurrentPlayer());

    }

    public void checkCross1() {
        for (int i = 0, j = 2; i < 3; i++, j--) {
            if (table[j][i] != getCurrentPlayer().getName()) {
                return;
            }
        }
        setFinish(true);
        setWinner(getCurrentPlayer());

    }

    public void checkCross2() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != getCurrentPlayer().getName()) {
                return;
            }
        }
        setFinish(true);
        setWinner(getCurrentPlayer());

    }

    public void checkDrew() {
        if (getRound() == 9 && getWinner() == null) {
            setFinish(true);
        }
    }
    
    public void clearTable(){
        setTable(new char[][]{{'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}});
    }

}
