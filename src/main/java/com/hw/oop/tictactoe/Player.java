/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hw.oop.tictactoe;

/**
 *
 * @author BenZ
 */
public class Player {
    private char name;
    private int win, lost, draw;
    
    public Player(char name) {
        setName(name);
        setWin(0);
        setLost(0);
        setDraw(0);
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public void setWin(int win) {
        this.win += win;
    }

    public int getLost() {
        return lost;
    }

    public void setLost(int lost) {
        this.lost += lost;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw += draw;
    }

    
    public String toString() {
        return "Player " +name + 
                "\nwin  = " + win + 
                "\nlost = " + lost + 
                "\ndraw = " + draw  ;
    }
    
    
    
}
